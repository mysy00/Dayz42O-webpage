<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Items Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for various usage messages
    |
    */

    'location' => 'Lokalizacja:',
    'engine' => 'Załóż toolbox i użyj engine.',
    'gascan' => 'Użyj go w pobliżu pojazdu.'

];
