@extends('layouts.app')

@section('content')
<main class="container-fluid">
  <section class="row">
    <article class="col-md-4 offset-md-1">
      <h3 class="news-title">Update 420.124</h3>
      <ul>
        <li>Fixes and improvements</li>
        <li>New commands</li>
        <li>Language System</li>
        <li>Divir Abuser</li>
        <li>Uwaga! Żółty żółw przepływa przez rzeke</li>
        <li>New Ganja for Haze</li>
        <li>New bed for Hini</li>
      </ul>

      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
    </article>

    <article class="col-md-4 offset-md-1">
      <h3 class="news-title">Update 420.124</h3>
      <ul>
        <li>Fixes and improvements</li>
        <li>New commands for admins</li>
      </ul>
    </article>

    <article class="col-md-4 offset-md-1">
      <h3 class="news-title">Update 420.122</h3>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      </p>
    </article>

    <article class="col-md-4 offset-md-1">
      <h3 class="news-title">Update 420.121</h3>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      </p>
    </article>

    <article class="col-md-4 offset-md-1">
      <h3 class="news-title">Update 420.120</h3>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      </p>
    </article>
  </section>
</main>
@endsection
