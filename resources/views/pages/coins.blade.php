@extends('layouts.app')

@section('content')
<main class="container">
  <section class="row">
    <article class="col-md-10">
    <h3 class="page-title">$ Coins $</h3>
    <p>@lang('coins.coinsInfo')</p>
    </article>
  </section>
</main>
@endsection
