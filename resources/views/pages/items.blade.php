@extends('layouts.app')

@section('content')
<main class="container">
  <section class="row">
    <!--<article class="col-lg-4 col-md-6 offset-md-1">
      <h3 class="item-title">Medkit</h3>
      <div class="item-image">
        <img src="http://placehold.it/300x250" class="img-fluid" alt="Hospitals Map">
      </div>
      <ul class="item-about">
        <li class="item-attr"><span class="item-desc-title">Location:</span><span class="item-desc">Hospitals</span></li>
        <li class="item-attr"><span class="item-desc-title">Description:</span><span class="item-desc">Cures bleeding, shock, broken leg and filles up your blood.</span></li>
        <li class="item-attr"><span class="item-desc-title">Usage:</span><span class="item-desc">Select by clicking on it and click "Use" button</span></li>
      </ul>
    </article>-->

    @foreach ($items as $item)
      <article class="col-lg-3 col-md-10 offset-md-1">
        <h3 class="item-title">{{ $item->name }}</h3>
        <div class="item-image">
          <img src="http://placehold.it/300x250" class="img-fluid" alt="{{ $item->name }}">
        </div>
        <ul class="item-about">
          <li class="item-attr"><span class="item-desc-title">@lang('itemsLocation.location')</span><span class="item-desc"><?php echo __('itemsLocation.'.$item->translation);?></span></li>
          <li class="item-attr"><span class="item-desc-title">@lang('itemsDescription.description')</span><span class="item-desc"><?php echo __('itemsDescription.'.$item->translation);?></span></li>
          <li class="item-attr"><span class="item-desc-title">@lang('itemsUsage.usage')</span><span class="item-desc"><?php echo __('itemsUsage.'.$item->translation);?></span></li>
        </ul>
      </article>
    @endforeach
  </section>
</main>
@endsection
