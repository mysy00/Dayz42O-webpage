@extends('layouts.app') @section('content')

<section>
  <div class="jumbotron jumbotron-fluid">
    <div class="container">
      <h1 class="display-3 text-center">Welcome to Dayz<span class="accent-color">42O</span></h1>
      <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
        dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
    </div>
  </div>

  <main class="server-info container">
    <section class="row justify-content-center">
      <div class="col-md-4">
        <blockquote class="blockquote">
          <p class="mb-0">SF is coming soon</p>
          <footer class="blockquote-footer">Few years ago said
            <cite title="Source Title">Pottus</cite>
          </footer>
        </blockquote>
      </div>
    </section>

    <section class="row">
      <div class="col server-tracker">
        <!--<img src="http://placehold.it/470x125" alt="Server Tracker">-->
        <img src="http://www.game-state.com/46.105.43.212:1420/560x95_FFFFFF_FF9900_000000_000000.png" alt="Server Tracker">
      </div>
    </section>
    <section class="row">
      <div class="col server-players">
        <h3 class="server-players-title">Players Online</h3>
        <ul class="server-players-list">
          <li><span class="samp-name" style="color:#FFD700">Diessel</span></li>
          <li><span class="samp-name" style="color:#7FFF00">Eloisa42O</span></li>
          <li><span class="samp-name" style="color:#00FFFF">Halo</span></li>
          <li><span class="samp-name" style="color:#0000FF">Jack42O</span></li>
          <li><span class="samp-name" style="color:#0000FF">John_Ley</span></li>
          <li><span class="samp-name" style="color:#0000FF">KuSH_</span></li>
          <li><span class="samp-name" style="color:#7FFF00">Lucian42O</span></li>
          <li><span class="samp-name" style="color:#2F4F4F">Metalfish</span></li>
          <li><span class="samp-name" style="color:#0000FF">Moby42O</span></li>
          <li><span class="samp-name" style="color:#FFD700">Nathan42O</span></li>
          <li><span class="samp-name" style="color:#7FFF00">Oscar.LS</span></li>
          <li><span class="samp-name" style="color:#DA70D6">Pumpkin</span></li>
          <li><span class="samp-name" style="color:#0000FF">Trissmoss</span></li>
          <li><span class="samp-name" style="color:#0000FF">W1z.42O)</span></li>
          <li><span class="samp-name" style="color:#7FFF00">[AM]Tamz42O</span></li>
          <li><span class="samp-name" style="color:#7FFF00">[HsC]42Q</span></li>
          <li><span class="samp-name" style="color:#87CEFA">[NsK]Creamy</span></li>
          <li><span class="samp-name" style="color:#FFD700">[SOA]Hertz</span></li>
          <li><span class="samp-name" style="color:#DA70D6">[uL]Bryle</span></li>
          <li><span class="samp-name" style="color:#FFFFFF">[uL]Wang</span></li>
          <li><span class="samp-name" style="color:#0000FF">busato</span></li>
          <li><span class="samp-name" style="color:#B22222">champagnepapi</span></li>
          <li><span class="samp-name" style="color:#FF4500">saarfJr</span></li>
          <li><span class="samp-name" style="color:#FFD700">twsh</span></li>
        </ul>
      </div>
    </section>
  </main>
</section>

@endsection
