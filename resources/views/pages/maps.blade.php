@extends('layouts.app')

@section('content')
<main class="container">
  <section class="row">
    <article class="col-lg-4 col-md-10 offset-md-1">
      <h3 class="map-title">Hospitals</h3>
      <a href="http://placehold.it/1440x900" target="_blank">
        <img src="http://placehold.it/1440x900" class="img-fluid" alt="Hospitals Map">
      </a>
    </article>
    <article class="col-lg-4 col-md-10 offset-md-1">
      <h3 class="map-title">Military</h3>
      <a href="http://placehold.it/1440x900" target="_blank">
        <img src="http://placehold.it/1440x900" class="img-fluid" alt="Hospitals Map">
      </a>
    </article>
    <article class="col-lg-4 col-md-10 offset-md-1">
      <h3 class="map-title">Water points</h3>
      <a href="http://placehold.it/1440x900" target="_blank">
        <img src="http://placehold.it/1440x900" class="img-fluid" alt="Hospitals Map">
      </a>
    </article>
    <article class="col-lg-4 col-md-10 offset-md-1">
      <h3 class="map-title">Worth to check out</h3>
      <a href="http://placehold.it/1440x900" target="_blank">
        <img src="http://placehold.it/1440x900" class="img-fluid" alt="Hospitals Map">
      </a>
    </article>
  </section>
</main>
@endsection
