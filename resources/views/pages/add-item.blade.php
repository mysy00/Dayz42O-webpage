@extends('layouts.app')

@section('content')
<main class="container">
  <section class="row">
    <article class="col-md-6 align-self-center text-center">
      <h3 class="item-title">Add item</h3>
      {{Form::open(array('url'=>'admin/add-item','method'=>'post'))}}
        <input type="text" class="form-control form-control-lg" name="itemName" placeholder="Item">
        <input type="text" class="form-control form-control-lg" name="translationName" placeholder="Translation Name">
        <input type="submit" class="btn btn-primary" value="Add it">
      {{Form::close()}}
    </article>
  </section>
</main>
@endsection
