@extends('layouts.app')

@section('content')
<main class="container">
  <section class="row">
    <article class="col-md-10 faq">
      <ol>
        <li>Question
          <ul>
            <li>Answer</li>
          </ul>
        </li>

        <li>Question2
          <ul>
            <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos eveniet architecto cumque, nostrum, consequatur suscipit a quis dolores nobis repellat explicabo odio numquam hic assumenda tempore at animi fuga aspernatur!</li>
          </ul>
        </li>

        <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis dicta placeat praesentium dolores eius nisi iste optio eaque error animi harum officiis molestiae illum corrupti dignissimos consectetur blanditiis, delectus ea!
          <ul>
            <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos eveniet architecto cumque, nostrum, consequatur suscipit a quis dolores nobis repellat explicabo odio numquam hic assumenda tempore at animi fuga aspernatur!</li>
          </ul>
        </li>
      </ol>
    </article>
  </section>
</main>
@endsection
