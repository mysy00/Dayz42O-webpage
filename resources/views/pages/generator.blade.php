@extends('layouts.app')

@section('content')
<main class="container">
  <section class="row">
    <article class="col-md-6">
      <div class="gen-config">
        <form id="imageData" method="get" action="gen.php">
          <div class="form-group">
            <input type="text" class="form-control form-control-lg" id="formNickname" name="nickname" placeholder="Nickname">
            <input type="text" class="form-control form-control-lg" id="formGang" name="gang" placeholder="Gang">

            <div class="image-type">
              <select id="imageType" class="custom-select" name="imageType"></select>
            </div>

            <select id="imagesList" class="image-picker" name="imageName"></select>
          </div>
        </form>
      </div>
    </article>

    <article class="col-md-5">
      <div class="image-address">
        <span class="js-clipboardCopy clipboard-copy badge badge-primary" data-clipboard-target="#imageAddress">Copy me!</span>
        <input id="imageAddress" class="form-control form-control-lg" type="text" value="http://d1vv.tk/generators/beta/gen.png?" readonly>
        <span id="imageAddress"></span>
      </div>
    </article>
  </section>

  <section class="row">
    <div class="col-md-12 text-center">
      <div id="imagePreview" class="image-preview"></div>
    </div>
  </section>
</main>

@endsection
