new Clipboard('.js-clipboardCopy');

// Refresh while typing
$(function() {
  var imageData       = $("#imageData"),
      imagePreview    = $("#imagePreview"),
      websiteAddress  = location.origin,
      imageAddress    = $("#imageAddress");

  var update = function() {
    var imageDataSerialized = imageData.serialize()
    imageAddress.val(
      websiteAddress + "/gen.php?" + imageDataSerialized
    );
    imagePreview.html('<img src=gen.php?' + imageDataSerialized + ' alt="Loading...">');
  };
  imageData.on('input', function() {
    update();
  });
  imageData.on('change', 'select', function() {
    update();
  });
})

// Generate select for available image types
var data = [ // The data
    ['Signatures', [
        'bg1','bg2', 'bg3', 'bg4'
    ]],
    ['Userbars', [
        'bg101', 'bg102', 'bg103', 'bg104'
    ]]
];

var imageType = $('#imageType'); // The dropdowns
var imagesList = $('#imagesList');

for(var i = 0; i < data.length; i++) {
    var first = data[i][0];
    imageType.append($("<option>"). // Add options
       attr("value",first).
       attr("title",first).
       data("sel", i).
       text(first));
}

imageType.change(function() {
    var index = $(this).children('option:selected').data('sel');
    var second = data[index][1]; // The second-choice data

    imagesList.html(''); // Clear existing options in second dropdown

    for(var j = 0; j < second.length; j++) {
        imagesList.append($("<option>"). // Add options
           attr("value",second[j]).
           attr("data-img-src", "images/generator/" + second[j] + ".png").
           data("sel", j).
           text(second[j]));
    }
    imagesList.imagepicker()

}).change(); // Trigger once to add options at load of first choice
