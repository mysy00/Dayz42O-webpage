<p align="center"><img src="https://i.imgur.com/Wqi6Xu0.png"></p>

Website created for Dayz42O samp server.

## Installation
1. Clone the project onto your server
```shell
$ git clone git@gitlab.com:Divosky/Dayz42O-webpage.git
```

2. Install the project
```shell
$ composer install
```

3. Configure `.env` file for your environment.
4. Configure `config/app.php` file.
5. Create tables
```shell
$ php artisan migrate
```

## Credits
- **[Divosky](https://nowitam.pl)**
- **[Laravel](https://laravel.com)**
- **[Bootstrap](https://getbootstrap.com)**

