<?php
	error_reporting(E_ALL);
	ini_set('display_errors','1');

	header('Content-type: image/png');

	$nickname = 'Nickname: ' . $_GET['nickname'];
	$gang = 'Gang: ';
	$grank = 'Gang Rank: 4';
	$bgnumber = '1';
	$level = 'Level: 41';
	$kills = '9251'; 					#count it
	$deaths = '4918';					#count it
	$ratio = round($kills/$deaths,2);	#count it
	$ekills = 'Kills: '.$kills;
	$edeaths = 'Deaths: '.$deaths;
	$eratio = 'Ratio: '.$ratio;
	$zkills = 'Zombie kills: 15414';

	$im = imagecreate(350, 19);
	//$bg = imagecolorallocate($im, 55, 55, 55);
	$bg = imagecreatefrompng('images/generator/bg'.$bgnumber.'.png');

	#colors_start
	$green = imagecolorallocate($bg, 0,255,0);
	$yellow = imagecolorallocate($bg, 255,215,0);
	$lightblue = imagecolorallocate($bg, 199, 249, 253);
	$white = imagecolorallocate($bg, 240, 240, 240);
	$black = imagecolorallocate($bg, 15, 15, 15);
	#colors_end

	$font = 'fonts/Lato-Regular.ttf';
	if ($bgnumber == 1) {
		imagettftext($bg, 10, 0, 50, 55, $green, $font, $nickname);
		imagettftext($bg, 10, 0, 50, 75, $green, $font, $gang);
		imagettftext($bg, 10, 0, 50, 95, $green, $font, $grank);
		imagettftext($bg, 10, 0, 50, 115, $green, $font, $level);
		imagettftext($bg, 10, 0, 50, 135, $green, $font, $ekills);
		imagettftext($bg, 10, 0, 50, 155, $green, $font, $edeaths);
		imagettftext($bg, 10, 0, 50, 175, $green, $font, $eratio);
		imagettftext($bg, 10, 0, 50, 195, $green, $font, $zkills);
	}
	elseif ($bgnumber <= '2' || $bgnumber <= '3'){ // #2, #2, #3 et cetera
		imagettftext($bg, 10, 0, 50, 55, $yellow, $font, $nickname);
		imagettftext($bg, 10, 0, 50, 75, $yellow, $font, $gang);
		imagettftext($bg, 10, 0, 50, 95, $yellow, $font, $grank);
		imagettftext($bg, 10, 0, 50, 115, $yellow, $font, $level);
		imagettftext($bg, 10, 0, 50, 135, $yellow, $font, $ekills);
		imagettftext($bg, 10, 0, 50, 155, $yellow, $font, $edeaths);
		imagettftext($bg, 10, 0, 50, 175, $yellow, $font, $eratio);
		imagettftext($bg, 10, 0, 50, 195, $yellow, $font, $zkills);
	}
	elseif ($bgnumber == '4'){
		imagettftext($bg, 10, 0, 25, 55, $lightblue, $font, $nickname);
		imagettftext($bg, 10, 0, 25, 70, $lightblue, $font, $gang);
		imagettftext($bg, 10, 0, 25, 85, $lightblue, $font, $level);
		imagettftext($bg, 10, 0, 225, 55, $lightblue, $font, $ekills);
		imagettftext($bg, 10, 0, 225, 70, $lightblue, $font, $edeaths);
		imagettftext($bg, 10, 0, 225, 85, $lightblue, $font, $eratio);
	}
	elseif ($bgnumber >='100'){
		imagettftext($bg, 10, 0, 6, 15, $black, $font, $nickname);
		imagettftext($bg, 10, 0, 5, 14, $white, $font, $nickname);
		imagettftext($bg, 10, 0, 151, 15, $black, $font, $gang);
		imagettftext($bg, 10, 0, 150, 14, $white, $font, $gang);
	}

	#imagettftext(resource $image, float $size, float $angle, int $x, int $y, int $color, string $fontfile, string $text)

	imagepng($bg);
	imagedestroy($bg);

?>
