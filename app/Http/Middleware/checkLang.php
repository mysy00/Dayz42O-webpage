<?php

namespace App\Http\Middleware;

use Closure;
use Cookie;

class checkLang
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {

        app()->setLocale(Cookie::get('locale'));

        return $next($request);
    }
}
