<?php

namespace App\Http\Controllers;

use App\items;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index()
    {
      return view('pages.index');
    }

    public function changelog()
    {
      return view('pages.changelog');
    }

    public function maps()
    {
      return view('pages.maps');
    }

    public function items()
    {
      $items = Items::all();
      return view('pages.items', compact('items'));
    }
    public function faq()
    {
      return view('pages.faq');
    }
    public function coins()
    {
      return view('pages.coins');
    }
    public function generator()
    {
      return view('pages.generator');
    }
    public function admin()
    {
      return view('pages.admin');
    }
    public function additem()
    {
      return view('pages.add-item');
    }
}
