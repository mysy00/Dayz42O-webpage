<?php

namespace App\Http\Controllers;

use Request;
use Redirect;
use Cookie;

class LangController extends Controller
{
    public function chooser()
    {
      #Session::put('locale', Request::get('locale'));

      $cookie = Cookie::make('locale', Request::get('locale'), 60);


      return Redirect::back()->cookie($cookie);
    }
}
